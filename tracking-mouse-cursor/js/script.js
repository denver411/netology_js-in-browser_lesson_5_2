'use strict'

document.addEventListener('mousemove', eyeAnimation);
const leftEye = document.querySelector('.cat_position_for_left_eye').getBoundingClientRect();
const rightEye = document.querySelector('.cat_position_for_right_eye').getBoundingClientRect();
const leftPupil = document.querySelector('.cat_eye_left');
const rightPupil = document.querySelector('.cat_eye_right');

function eyeAnimation(event) {

if (event.pageX > (leftEye.right - leftPupil.offsetWidth)){
  leftPupil.style.left = `${leftEye.width - leftPupil.offsetWidth}px`;
} else if (event.pageX < leftEye.left){
  leftPupil.style.left = `0px`;
} else {
  leftPupil.style.left = `${leftEye.width / 2 - leftPupil.offsetWidth / 2}px`;
}

if (event.pageY > (leftEye.bottom - leftPupil.offsetHeight)){
  leftPupil.style.top = `${leftEye.height - leftPupil.offsetHeight}px`;
} else if (event.pageY < leftEye.top){
  leftPupil.style.top = `0px`;
} else {
  leftPupil.style.top = `${leftEye.height / 2 - leftPupil.offsetHeight / 2 }px`;
}

if (event.pageX > (rightEye.right - rightPupil.offsetWidth)){
  rightPupil.style.left = `${rightEye.width - rightPupil.offsetWidth}px`;
} else if (event.pageX < rightEye.left){
  rightPupil.style.left = `0px`;
} else {
  rightPupil.style.left = `${rightEye.width / 2 - rightPupil.offsetWidth / 2}px`;
}

if (event.pageY > (rightEye.bottom - rightPupil.offsetHeight)){
  rightPupil.style.top = `${rightEye.height - rightPupil.offsetHeight}px`;
} else if (event.pageY < rightEye.top){
  rightPupil.style.top = `0px`;
} else {
  rightPupil.style.top = `${rightEye.height / 2 - rightPupil.offsetHeight / 2 }px`;
}


}