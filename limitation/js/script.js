'use strict'

function debounce(callback, delay) {
  let timeout;
  return () => {
    clearTimeout(timeout);
    timeout = setTimeout(function () {
      timeout = null;
      if (input === document.activeElement) {
        callback();
      }
    }, delay);
  };
};

const input = document.querySelector('.textarea');

input.addEventListener('keydown', debounce(() => {
  document.querySelector('.block').classList.remove('active');
  document.querySelector('.message').classList.add('view');
}, 2000));

input.addEventListener('keydown', () => {
  document.querySelector('.block').classList.add('active');
  document.querySelector('.message').classList.remove('view');
});

input.addEventListener('focus', () => {
  document.querySelector('.block').classList.add('active');
});

input.addEventListener('focus', debounce(() => {
  document.querySelector('.block').classList.remove('active');
}, 2000));

input.addEventListener('blur', () => {
  document.querySelector('.block').classList.remove('active');
  document.querySelector('.message').classList.remove('view');
});