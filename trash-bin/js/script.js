'use strict'

let movedPiс;

document.addEventListener('mousedown', (event) => {
  if (event.target.classList.contains('logo')) {
    movedPiс = event.target;
  }
})

document.addEventListener('mousemove', event => {
  if (movedPiс) {
    event.preventDefault();
    movedPiс.style.left = event.pageX - movedPiс.width / 2 + 'px';
    movedPiс.style.top = event.pageY - movedPiс.height / 2 + 'px';
    movedPiс.classList.add('moving');
  }
});

document.addEventListener('mouseup', event => {
  if (movedPiс) {
    const targetBlock = document.elementFromPoint(event.clientX, event.clientY);
    if (targetBlock === document.getElementById('trash_bin')) {
      // targetBlock.appendChild(movedPiс);
      movedPiс.classList.remove('moving');
      movedPiс.style.display = 'none';
      movedPiс = null;
    } else {
      movedPiс.classList.remove('moving');
      movedPiс.style.left = event.pageX - movedPiс.width / 2 + 'px';
      movedPiс.style.top = event.pageY - movedPiс.height / 2 + 'px';
      movedPiс = null;
    }
  }
});